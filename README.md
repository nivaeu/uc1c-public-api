# $` \textcolor{red}{\text{This API and repository is deprecated!}} `$
Relevant repositories are https://gitlab.com/nivaeu/uc1c-backend and https://gitlab.com/nivaeu/uc1c-ui

# Introduction
This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for further information.
A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

# NIVA UC1c Public API
## Description
Integration with Estonian registry.

Deployed at [Heroku](https://uc1c-public-api.herokuapp.com/)

Build with [Nest](https://github.com/nestjs/nest), a  TypeScript framework.


## Installation

```bash
$ npm install
```

## Running the app

Development uses [prettier logs](https://github.com/pinojs/pino-pretty).
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
Open browser and see swagger on [localhost](http://localhost:3000) .

## How to start the mock service as a container

Required software:
- docker (Docker Desktop is fine. https://www.docker.com/products/docker-desktop)
- git

Commands to start the application (Powershell must be used on Windows):

```bash
docker build -f ./docker/Dockerfile -t niva/uc1c-public-api:latest .
docker run --name niva --publish 127.0.0.1:8080:3000 --rm -it niva/uc1c-public-api
```

Browse to http://localhost:8080


## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## License
EU-PL
Nest framework is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).
