/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { HttpModule, Module } from '@nestjs/common';
import { KlientService } from './klient.service';
import { PoldService } from './pold.service';
import { PriaClient } from './pria.client';

@Module({
  imports: [HttpModule],
  providers: [KlientService, PoldService, PriaClient],
  exports: [KlientService, PoldService, PriaClient],
})
export class PriaModule {}
