/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

export interface Pold {
  WKT: string;
  AASTA: number;
  POLLU_ID: number;
  KLIENT_KOOD: number;
  POLLU_PIND: any;
  MUUTMISE_AEG: string;
  POLLUKULTUUR_KOOD: string;
  NIITMISE_TUVASTAMINE_STAATUS: string;
  NIITMISE_VARASEIM_AEG: string;
  NIITMISE_HILISEIM_AEG: string;
}
