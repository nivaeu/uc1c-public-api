/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

export interface Klient {
  id: number;
  muutja: string;
  muutmisAeg: string;
  algusAeg: string;
  loppAeg?: any;
  alusdokument?: any;
  muuAlus: string;
  klrId: number;
  onJuriidilineIsik: boolean;
  ettevotlusvormKlf?: any;
  eesnimi: string;
  perekonnanimi: string;
  synniaegKp: string;
  isikukood: string;
  isikukoodRiikKlf: string;
  suguKlf: string;
  juriidilineNimi?: any;
  registrikood?: any;
  registrikoodRiikKlf: string;
  majandusaastaAlgusKp?: any;
  ariregisterStaatus?: any;
  ariregisterStaatusAeg?: any;
  staatusKlf: string;
  staatusAeg: string;
  onLubatudRegisterAadressKasutamine?: any;
  lisaja: string;
  andmeteKontrollimiseAeg?: any;
  elektroonilineSuhtlus: boolean;
}
