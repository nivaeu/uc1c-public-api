/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { Injectable } from '@nestjs/common';
import { Pold } from './interfaces/pold.interface';
import { PriaClient } from './pria.client';
import { PinoLogger } from 'nestjs-pino';

@Injectable()
export class PoldService {
  constructor(private readonly logger: PinoLogger, private priaClient: PriaClient) {
    this.logger.setContext('PoldService');
  }

  async findByClient(clientId: number): Promise<Pold[]> {
    this.logger.info(`Requesting fields for clientId: ${clientId}`);
    const url = 'https://devpms.arib.pria.ee/pold/ext/pollud/list';
    const response = await this.priaClient.get(url, { params: { klientId: clientId, pageNumber: 1, pageSize: 5 } });
    const pollud = response.data;
    this.logger.info(`Retrieved ${pollud.length} fields`);
    return pollud;
  }
}
