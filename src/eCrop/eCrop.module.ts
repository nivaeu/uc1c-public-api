/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { Module } from '@nestjs/common';
import { CropPlotController } from './cropPlot.controller';
import { PriaModule } from '../pria/pria.module';
import { CropPlotService } from './cropPlot.service';

@Module({
  providers: [CropPlotService],
  imports: [PriaModule],
  controllers: [CropPlotController],
})
export class ECropModule {}
