/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { ApiProperty } from '@nestjs/swagger';
import { SpecifiedPartyInterface } from './SpecifiedParty.interface';
import { IsDateString } from 'class-validator';

export class CropReportDocument {
  @ApiProperty({ example: 12345, description: 'Document ID' })
  ID?: number;
  @ApiProperty()
  @IsDateString()
  IssueDateTime?: string;
  @ApiProperty()
  @IsDateString()
  CopyIndicator?: string;
  @ApiProperty()
  ControlRequirementIndicator?: string;
  @ApiProperty()
  SenderSpecifiedParty: SpecifiedPartyInterface;
  @ApiProperty()
  RecipientSpecifiedParty: SpecifiedPartyInterface;
}
