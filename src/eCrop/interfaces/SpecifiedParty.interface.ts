/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsInt, IsOptional, IsString } from 'class-validator';

export class SpecifiedPartyInterface {
  @ApiProperty({ example: 1, examples: [1, 2], description: 'Recipient ID. PRIA 1, eAgronom 2' })
  @IsInt()
  @IsIn([1, 2])
  ID: number;
  @ApiProperty({ description: 'Recipient Name', required: false })
  @IsString()
  @IsOptional()
  Name?: string;
}
