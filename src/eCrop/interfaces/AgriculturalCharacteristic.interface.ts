/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { ApiProperty } from '@nestjs/swagger';
import { CodeType } from './CodeType.interface';

export class AgriculturalCharacteristic {
  @ApiProperty({required: false})
  TypeCode?: CodeType;
  @ApiProperty({required: false})
  ValueCode?: CodeType;
  @ApiProperty({required: false})
  ValueIndicator?: boolean;
}
