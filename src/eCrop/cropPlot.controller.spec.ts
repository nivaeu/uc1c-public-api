/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { CropPlotController } from './cropPlot.controller';
import { CropPlotService } from './cropPlot.service';
import * as poldInfo from '../../test/mocks/pold_info.json'
import * as works from '../../test/mocks/works.json'
import { LoggerModule } from 'nestjs-pino';
import { Test, TestingModule } from '@nestjs/testing';
import { PriaModule } from '../pria/pria.module';
import { ConfigModule } from '@nestjs/config';
import configuration from '../configuration';
import { ECropModule } from './eCrop.module';

describe('CropPlotController', () => {
  let catsController: CropPlotController;

  beforeEach(async () => {
    let module: TestingModule;
    module = await Test.createTestingModule({
      imports: [LoggerModule.forRoot(), ECropModule, PriaModule,  ConfigModule.forRoot({
        load: [configuration],
        isGlobal: true,
      })],
      providers: [CropPlotController, CropPlotService],
    }).compile();

    catsController = module.get<CropPlotController>(CropPlotController);
  });

  describe('create', () => {
    it('should pass for object with field info', async () => {
      expect(await catsController.create(poldInfo)).toBe(undefined);
    });

    it('should pass for object with works info', async () => {

      expect(await catsController.create(works)).toBe(undefined);
    });
  });
});
