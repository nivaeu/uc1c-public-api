/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

export default () => ({
  cas: {
    url: 'https://devpms.arib.pria.ee/cas/v1/tickets',
    name: 'https://devpms.arib.pria.ee/pms-menetlus/',
    userName: process.env.CAS_USERNAME,
    password: process.env.CAS_PASSWORD,
  },
});
