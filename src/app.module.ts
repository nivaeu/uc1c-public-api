/**
 * Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2021.
 * This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 */

import { Module } from '@nestjs/common';
import { ECropModule } from './eCrop/eCrop.module';
import configuration from './configuration';
import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [ECropModule,
    LoggerModule.forRoot({
      pinoHttp: {
        mixin() {
          return { module: 'NIVA' };
        },
      },
    }),
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    })],
})
export class AppModule {}
